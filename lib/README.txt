Icons:

All icons here are under the GPL. I am including the photoshop (.psd) file for all icons I made myself,
or pulled from the lullabot set.  Please feel free to use these icons in your own projects, and/or
improve them and send them to me!

famfamfam mini set: clone

iconify alpha3: delete, edit, locked

tao: close_menu, open_menu, colapsed, expanded, hidden, visible
