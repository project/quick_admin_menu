// *******************************************************************************
// ***  QAM (Quick Admin Menu) - Sortable Tree Behaviors  ************************
// *******************************************************************************

var QAM = QAM || {};

(function($){


var	base_path;
var	module_path;
var current_dropzone;
var last_dropzone;
var current_dropmode = 'in';
var console = console || { log: function(){} }; // turn off messges
var DOASSERTS = true;

function asrt( test, msg ) {
	if( DOASSERTS && !test ) {
		throw new Error( 'Assert Failed: ' + msg );
	}
}

/**
 *	toggle_branch:
 *		Handles the click on + or - icon
 */
QAM.toggle_branch = function() {
	var mode = $(this).attr('children');
	var subbranch = $('ul', this.parentNode.parentNode).eq(0);
	console.log( mode );
	switch( mode ) {
		case 'closed':
			$(this).attr('children', 'open').hide().show(); // hide.show wakes up ie7
		  subbranch.show();
			break;
		case 'open':
			$(this).attr('children', 'closed').hide().show();
		  subbranch.hide();
			break;
	}
};

/**
 *	prune_branch:
 *		Test for removing parentBranch's last child ( remove ul & minus sign)
 *	@branch: ul, branch.parentNode: li
 */
QAM.prune_branch = function( branch ) {
//console.log( "Prune Branch: ");
//console.log(branch);

	var oldBranches = $('li', branch);
	if (oldBranches.size() === 1) { /* jquib, changing 0 to 1 */
	  // remove +/- button
		$('div.qam-expander', branch.parentNode).attr('children', 'none');
		
		//remove expand/collapse button
		$('div.text-holder', branch.parentNode).attr( 'expanded', 'none' );
//		console.log( 'prune_branch: expaned set to none');
		$('div.item-option-expanded', branch.parentNode).attr('title', $('#qam-expanded-none-message').val() );
		
		$(branch).remove();
	}
};

/**
 *	sync_spacing_to_sibling:
 *		Sync dropped_item's space width with sibling
 *	@sibling: li - above or below
 *	@dropped_item: li being dropped
 */
QAM.sync_spacing_to_sibling = function( sibling, dropped_item ) {
	var px = $('div.qam-expander', sibling).eq(0).css('width');
	var width = px.substr(0, (px.length -2) );
	$('div.qam-expander', dropped_item).eq(0).css( 'width', width + 'px');
	console.log( "sync_spacing_to_sibling - width: " + width );
	QAM.sync_spacing_of_children(dropped_item);
};

/**
 *	sync_spacing_to_parent:
 *		Sync dropped_item's space width with parent.
 *	@newParent: li (don't use variable name "parent" to avoid firebug bug).
 *	@dropped_item: li
 */
QAM.sync_spacing_to_parent = function( newParent, dropped_item ) {
	var px = $('div.qam-expander', newParent).eq(0).css('width');
	var w = px.substr(0, (px.length -2) );
	var width = 16 + Number( w );
	$('div.qam-expander', dropped_item).eq(0).css( 'width', width + 'px' );
	console.log( "sync_spacing_to_parent - width: " + width + 'px' );
	QAM.sync_spacing_of_children(dropped_item);
};

// sync dropped_item's children li's
QAM.sync_spacing_of_children = function(item) {
	$('ul', item).eq(0).children().each( function(){ QAM.sync_spacing_to_parent( item, this ); } );
};


/**
 *	start_expanding_branch:
 *		Start timer to expand a hidden branch of menu tree
 *	@dropzone: the branch
 */
QAM.start_expanding_branch = function(dropzone) {
  return; // not working
//  console.log( "QAM.Start Expanding Branch: " + dropzone );
  var targetBranch;
  var subbranches;
  var subbranch;
//  console.log( "QAM.Start Expanding Branch: Expanded? " + dropzone.expanded );
	if (!dropzone.expanding) {
		subbranches = $('ul', dropzone.parentNode);
		if (subbranches.size() > 0) {
			subbranch = subbranches.eq(0);
			dropzone.expanding = true;			
//  console.log( 'start_expanding_branch: dropzone.expaned set to true');			
//  console.log( "QAM.Start Expanding Branch 3: " + subbranch.css('display') );
			if (subbranch.css('display') == 'none') {
				targetBranch = subbranch.get(0);
				dropzone.expanderTime = window.setTimeout( 
				  function(){ QAM.expand_branch(targetBranch, dropzone); }, 500 
				);
			}
		}
  }
};

QAM.expand_branch = function( branch, dropzone ) {
//console.log( "QAM.expand_branch: " + branch);
	$(branch).show(); // make the dropzone's children visible
	// show the - if there are children ???
	$('div.qam-expander', branch.parentNode).eq(0).attr('children', 'open');
	dropzone.expanding = false; // expanding happened, so reset flag on dropzone
//	$.recallDroppables();  removed interface -> jqui
};

/**
 *	cancel_expanding_branch:
 *		Cancel expander timer
 *	@dropzone: the branch
 */
QAM.cancel_expanding_branch = function(dropzone) {
console.log( "QAM.cancel_expanding_branch: " + dropzone );
	if (dropzone && dropzone.expanderTime){ // causing errors - jquib
		window.clearTimeout(dropzone.expanderTime);
		dropzone.expanding = false;
console.log( "QAM.cancel_expanding_branch: dropzone.expanding set to false" );
	}
};

/**
 *	hover_on_item:
 *		Trigged when dragged item moves in or out of a dropzone
 *	@dragged_item: item moved over zone
 *	@dropzone: item being hovered over
 *		
 *	sets: current_dropzone
 *	sets:	last_dropzone
 */
QAM.hover_on_item = function(dragged_item, dropzone) {
	QAM.clear_markers(dropzone); // need to have this here, to delete markers on super-fast movement
	last_dropzone = current_dropzone;
	current_dropzone = dropzone; // store for later use.
	console.log( "Current Dropzone = " + $('span a', current_dropzone).text() );
	
	// force the marker to be redrawn at the correct place
	current_dropmode = null;
	QAM.dragging_item(dragged_item); // jquib
};


/**
 * 	set_markers: 
 *		Set visual markers
 * 	@dropzone: div.text-holder
 *	@dropmode: 'in', 'above', 'below'
 */
QAM.set_markers = function(dropzone, dropmode) {
	switch(dropmode) {
		case 'in':
			$('span.item-title', dropzone).css( 'background', '#bfb' ).css( 'border', '1px dashed #4f4' );	
			break;
		case 'above': 
			$(dropzone).css( 'border-top', '2px dashed green' );				
			break;
		case 'below': 
			$(dropzone).css( 'border-bottom', '2px dashed green' );				
			break;
	}
};

/**
 * 	clear_markers: 
 *		Clear all visual markers
 */
QAM.clear_markers = function(dropzone) {
	if( dropzone ) {
		$('span.item-title', dropzone).css( 'background', '#ddd' ). css( 'border', '0' );
		$(dropzone).css( 'border', '1px solid #eee' ); 
	}
	if( last_dropzone ) { // using global, kind sloppy
		$(last_dropzone).css( 'border', '1px solid #eee' );	
	}					
};

/**
 * 	dragging_item: 
 *		the dragged item is being moved = set the current_dropzone and visual markers
 *	@x,y: not useful, not used
 *
 *	@sets: current_dropzone
 */
QAM.dragging_item = function( dragged_item ) { // x,y not useful
	var new_dropzone_mode = current_dropmode;
	var offset;
	
	// Test for three zones (bottom, middle & top)
	if( current_dropzone ) {
//console.log( "dragging item x: " + x + ", y: " + y );
// jquib - unfortunately, this bit of Interface codes does not translate to jQuery UI
//		offset = jQuery.iDrag.dragged.dragCfg.currentPointer.y - current_dropzone.dropCfg.p.y;
//console.log( dragged_item.pageY );
//console.log( $(current_dropzone).offset().top );
    offset = dragged_item.pageY - $(current_dropzone).offset().top; // jquib
//console.log( "Offset: " + offset );
    if( offset < 5 ) { 
    	new_dropzone_mode = 'above';
    }
//    else if( offset > current_dropzone.dropCfg.p.hb - 5 ) { // jquib
    else if( offset > $(current_dropzone).innerHeight() - 5 ) {
    	new_dropzone_mode = 'below';
    }
    else {
    	new_dropzone_mode = 'in';
    }
	}
	
	// if we are moving from one mode to another
	if( new_dropzone_mode != current_dropmode ) {
		current_dropmode = new_dropzone_mode;
//		console.log( "Changing Drop mode to " + new_dropzone_mode );
	
	  QAM.clear_markers(current_dropzone);
	  if(current_dropzone.parentNode != this) { // don't set markers if we are over the starting item
		  // Set the markers
		  QAM.set_markers(current_dropzone, current_dropmode);
		}
	}
};

/**
 * 	drop_item: 
 *		The dragged item has been dropped somewhere
 */
QAM.drop_item = function(dropped_item) { 
	console.log( "dropping_item item: " + $('span a', dropped_item).text() );
	console.log( current_dropmode + " item: " + $('span a', current_dropzone).text() );

  QAM.cancel_expanding_branch( current_dropzone );
  QAM.clear_markers(current_dropzone);
	if(current_dropzone && current_dropzone.parentNode != dropped_item) { // don't process drop if dropping on self (quib)
		switch(current_dropmode) {
			case 'in':
				QAM.drop_in_child(dropped_item, current_dropzone);
				break;
			case 'above': 
				QAM.drop_sibling_above(dropped_item, current_dropzone);
				break;
			case 'below': 
				QAM.drop_sibling_below(dropped_item, current_dropzone);
				break;
		}	
	}
};

/**
 * 	drop_in_child:
 *  	The dropped item will become the 1st child of the current_dropzone
 *	@dropped_item: li.tree-item
 *  @dropzone: dev.text-holder
 */
QAM.drop_in_child = function(dropped_item, dropzone) {
	var zoneParent = dropzone.parentNode; // ul above zone
	var subbranch = $('ul', zoneParent); // ul that is child of zone
	if (subbranch.size() === 0) { // if there is no ul as child of zone, add it in as the container for dropped_item 
		$(dropzone).after('<ul></ul>');
		subbranch = $('ul', dropzone.parentNode);
	}
	var oldParent = dropped_item.parentNode;
	subbranch.eq(0).prepend(dropped_item); // add dropped item as child of zone
	QAM.sync_spacing_to_parent( zoneParent, dropped_item );	//TODO 
	QAM.prune_branch( oldParent );

	var expander = $('div.qam-expander', zoneParent).eq(0);
	if (expander.attr('children') == 'none') {
		expander.attr('children', 'open');
		// adding 1st child, so adding expand/collapse button - defaulting to collapsed
		$(dropzone).attr( 'expanded', 'off' );
//		console.log( 'drop_in_child: dropzone.expanded set to off' );
		$('div.item-option-expanded', dropzone).attr('title', $('#qam-expanded-off-message').val() );
	}
};

/**
 *	drop_sibling_above:
 *		Dropped above = item will become the sibling above the current_dropzone
 * 	@dropzone = div.text-holder (dropzone.parentNode = li.tree-item being dropped on)
 *	@dropped_item = li.tree-item being dropped
 */
QAM.drop_sibling_above = function(dropped_item, dropzone) {
	var sibling = dropzone.parentNode; // get the dropzone's containing li
	var oldParent = dropped_item.parentNode;
	QAM.sync_spacing_to_sibling( sibling, dropped_item );
	// sync dropped_item's space width with sibling
	$(sibling).before(dropped_item);
	QAM.prune_branch( oldParent );
};

/**
 *	drop_sibling_below:
 *		Dropped below = item will become the sibling below the current_dropzone
 * 	@dropzone = div.text-holder
 *	@dropped_item = li.tree-item being dropped
 */
// this can be a little tricky:
// drop below leaf = insert after (just like drop above)
// drop below parent = insert before first child
QAM.drop_sibling_below = function(dropped_item, dropzone) {
	var sibling = dropzone.parentNode; // get the dropzone's containing li
	var oldParent = dropped_item.parentNode;
  var $subbranch = $( 'ul', sibling );
//	if( $subbranch.size() == 0 ) {
		QAM.sync_spacing_to_sibling( sibling, dropped_item );		
		$(sibling).after(dropped_item);
//	}
//	else { // sibling is already a parent, so drop into it
//		$subbranch.eq(0).prepend(dropped_item);
//		QAM.sync_spacing_to_parent( sibling, dropped_item );		
//	}
  QAM.prune_branch( oldParent );
};

QAM.close_dialog = function() {
	$('#dialog').remove();
	$('#overlay').remove();
};

QAM.submit_edit_dialog = function( item_link ) {
	var new_label = $('#dialog input[@name=edit_label]').val();
	var new_href = $('#dialog input[@name=edit_url]').val();
	var new_desc = $('#dialog input[@name=edit_title]').val();
	item_link.text( new_label );
	item_link.attr('href', new_href);
	item_link.attr('title', new_desc);
//	console.log( "submit: " + new_label + " " + new_url + " " + new_desc );
	QAM.close_dialog();
};

// button: the div of attribute being toggled
// attribute: 'expanded' or 'enabled'
QAM.toggle_attribute = function( button, attribute ) {
	var div = $(button.parentNode.parentNode);
	var status = 'on';
	if( div.attr( attribute ) == 'on' ) {
		status = 'off';
	}
	// pull the translated title out of the page structure
	var id = '#qam-message-' + attribute + '-' + status;
	$(button).attr('title', $(id).val() ); // update title
	// update attribute
	div.attr( attribute, status ).hide().show(); // hide.show to wake up ie7
};

// register click on one of the option buttons
// this = div.item-option-x, this.parentNode = span.item-options, this.p.p = div.text-holder, this.p.p.p = li
QAM.option_click = function() {
//	console.log( this );
	switch( $(this).attr('class') ) {
		case 'item-option-enabled':
			QAM.toggle_attribute( this, 'enabled' );		
			break;
		case 'item-option-expanded':
			QAM.toggle_attribute( this, 'expanded' );
			break;
		case 'item-option-delete': 
			QAM.toggle_attribute( this, 'deleted' );
			$(this.parentNode.parentNode.parentNode).hide().show(); // bump ie7
			// TODO - toggle on children as well
			// if 
/*		
			var li = this.parentNode.parentNode.parentNode;
			var pa = li.parentNode; // ul
			$(li).remove();
			QAM.prune_branch( pa );
*/			
			break;
		case 'item-option-edit': 
			var item_link = $('.item-title a', this.parentNode.parentNode);
			// pull the base_path off the front of the url, b/c it gets added in again on rendering
//			var href = item_link.attr('href');
//			if( href.indexOf( base_path ) != -1 ) {
//				href = href.substring( base_path.length, href.length );
//			}
//			QAM.edit_item(item_link, item_link.text(), href, item_link.attr('title'));
        QAM.edit_link( item_link );
			break;
		case 'item-option-clone': 
			var item = this.parentNode.parentNode.parentNode; // li
			asrt( $(item).is( '.tree-item' ), "option_click: Sorry bad class" );
//			console.log( item );		
			var clone = item.cloneNode( true );
			var link = $('.item-title:first a', clone);
			link.text( 'Copy of ' + link.text() );
			QAM.attach_behaviors( $(clone) ); // attach behaviors to clone
			$(item).after(clone);
			break;
		default: 
			break;
	}				
};

QAM.edit_link = function( link ) {
	// pull the base_path off the front of the url, b/c it gets added in again on rendering
	var href = link.attr('href');
	if( href.indexOf( base_path ) != -1 ) {
	  href = href.substring( base_path.length, href.length );
	}
	QAM.edit_item(link, link.text(), href, link.attr('title'));
}

QAM.edit_item = function(item, label, href, description) {
  //console.log( "QAM.edit_item" );
			var edit_label = '<div>Label: <input type="texfield" name="edit_label" value="'+ label +'"></div>';
			// TODO - strip the base_url out
			var edit_url = '<div>URL: <input type="texfield" name="edit_url" value="'+ href +'"> <a href="'+ base_path + href + '">Go To Page</a> </div>';
			var edit_title = '<div>Description: <input type="texfield" name="edit_title" value="'+ description +'"></div>';

		  var dialog_width = 320;
  		var dialog_height = 128;
  		var overlay = $('<div id="overlay"></div>');
  		overlay.css( 'opacity', '0.4' ); // for ie
  		$('body').prepend( overlay );
  		
  		var left = ( overlay.width() / 2 ) - ( dialog_width / 2 );
  		var top = ( overlay.height() / 2 ) - ( dialog_height / 2 );
  		var dialog = '<div style="left:'+ left +'px; top:'+ top +'px; width:'+ 
  			dialog_width +'px; height:'+ dialog_height +'px;" id="dialog" >';
			dialog += '<div id="dialog_title">Edit Menu Item: '+ label +'</div>';
  		dialog +=	edit_label + edit_url + edit_title;
			dialog += '  <div>';
  		dialog += '    <input type="button" value="Submit" id="dialog_submit" />';
  		dialog += '    <input type="button" value="Cancel" id="dialog_cancel" />';
			dialog += '  </div>'; // end buttons
  		dialog +=	'</div>'; // close dialog
  		overlay.before( dialog );
  		$('#dialog_submit').click( function(){ QAM.submit_edit_dialog(item); } );
  		$('#dialog_cancel').click( QAM.close_dialog );
};

QAM.add_new_item = function() {
//	alert( "TODO" );
  
	var content_wrapper = this.parentNode.parentNode.parentNode.parentNode;
	asrt( $(content_wrapper).is('div.content'), 'add_new_item: content_wrapper has moved');
	var tree = $('div.qam-item-tree', content_wrapper);
	asrt( tree.size(), 'add_new_item: could not find item_tree' );
	// get the first ul.dndTree, and if it doesn't exist, add it.
	var top = $('ul.dndTree', tree);
	if( !top.size() ) {
		top = tree.prepend( '<ul class="dndTree"></ul>' ).children()[0];
	}
	
	$.post( 
			base_path + 'admin/build/qam/get_new_item_js', null,
		 	function(item){ 
		 		var li = $(top).prepend(item).children()[0];  
				QAM.attach_behaviors( $(li) );
		 		var item_link = $('.item-title a', li);
		 		QAM.edit_item(item_link, 'New Item', 'new_url', 'new item' );
		 	}
	);
	return false;
};

// this = submit button 
QAM.submit_menu = function() {
//	console.log( 'Submitting menu ' + $(this).attr('mid') );
	var menu = $(this).prev()[0];
	var clone;
	var menu_html;
	asrt( $(menu).is('div.qam-item-tree'), "submit_menu: didn't find menu" );
	if( menu ) {
/*	
		// TODO - can I do this with filter, not clone/remove?
		clone = menu.cloneNode(true);
		$('img', clone).remove(); // remove the images, they are clutter and not xml friendly
		menu_html = '<div>'+ $(clone).html() + '</div>';
//		$(menu).filter('img');
//		var menu_html = '<div>'+ $(menu).filter('img').html() + '</div>';
*/
		menu_html = '<div>'+ $(menu).html() + '</div>';
  	console.log( menu_html );
		$('body').css( 'cursor', 'wait' );

		$.ajax( 
		  {
				type: "POST",
				url: base_path + "admin/build/qam/menu/update_js",
				data: { mid : $(this).attr('mid'), menu_html : menu_html },
				success: function(data){ alert("Success: " + data); $('body').css( 'cursor', 'default' ); },
				error: function(xhr, err_str, err) { alert( "Error: " + err_str ); $('body').css( 'cursor', 'default' ); }
			} 
		);

/*		$.post( 
			base_path + "admin/build/qam/menu/update_js", 
			{ mid : $(this).attr('mid'), menu_html : menu_html },
//			function(){ alert("Menu Updated.  You will need to do a page reload to see new menu in blocks"); }
//			function(reply){ alert(reply); QAM.highlight_message( 'foo' ); $('body').css( 'cursor', 'default' );
			function(reply){ alert(reply); $('body').css( 'cursor', 'default' ); }
		);
*/
  }
  return false;
};

/**
 *  Pop a message up at the top of the screen for a second
 */
QAM.highlight_message = function(msg) {	
	var msg_area = $('#qma-message-area');
	if( !$('#qma-message-area').size() ) {
		$('body').prepend( '<div id="qma-message-area"></div>' );
	}
	var width = 400;
	var left = $('body').width() / 2 - width/2;
	$('#qma-message-area').text( msg ).css( 'left', left ).
		slideDown('slow', function(){ setTimeout( function(){ $('#qma-message-area').slideUp('slow'); }, 1500 ); } );	
};

/**
 *	set_up_sortable_menu:
 *		scan the tree and set it up as a sortable menu
 */
QAM.set_up_sortable_menu = function() {
	base_path = Drupal.settings.qam.base_path;
	module_path = base_path + Drupal.settings.qam.module_path;
	
	var tree = $('ul.dndTree');
	QAM.attach_behaviors(tree);	
	$('input.qam_update').click( QAM.submit_menu );
	$('a.add_item_to_menu').click( QAM.add_new_item );
	
};

QAM.attach_behaviors = function( tree ) {  	
  $('div.text-holder', tree).css( 'cursor', 'N-resize' );	
  $('span.item-options div', tree).click( QAM.option_click );
		
  // code for expanding hidden leaves - 'this' is the image 
  $('div.qam-expander', tree.get(0)).click( QAM.toggle_branch );

  // configure the droppable behavior on items
  $('div.text-holder', tree).droppable(
			{
				accept			  : '.tree-item',
				tolerance			: 'pointer',
				over			  : function(dragged) { 
					QAM.start_expanding_branch(this);
					QAM.hover_on_item(dragged, this);
				},
				out					: function() {
					QAM.cancel_expanding_branch(this);
				}
			}
  );
		
  var drag_options = {
				revert		: false,
				autoSize	: false,
				ghosting	: true,
				opacity   : 0.5,
				axis			: 'vertically',
				// negative is up.  0,0 is the point where I start?!  Not sure if context is used
				drag		: function(dragged) { QAM.dragging_item(dragged); /* jquib QAM.dragging_item(x,y); */ }, 
				stop		: function() { QAM.drop_item(this); }
  };
				
	// configure the draggable behavoir on items
	if( tree.is('.tree-item') ) { 
		tree.draggable( drag_options );	// special case, add behavior directly to branch		
	}
	$('li.tree-item', tree).draggable( drag_options );		
		
	// make the menu item itself act as an edit link
  $('span.item-title a', tree).click( function(){QAM.edit_link($(this)); return false} );
		
};


})($ui);

if( Drupal.jsEnabled ) {
  $ui(document).ready(QAM.set_up_sortable_menu);
}

